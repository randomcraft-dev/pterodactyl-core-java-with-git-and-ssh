#{{CLONE_URL}} {{SERVER_JARFILE}}
if [ ! -z "${SSH_KEY}" ]; then
rm -rf ~/.ssh/id_rsa
mkdir -p ~/.ssh
cat >~/.ssh/config <<EOL
Host *
    StrictHostKeyChecking no
EOL
SSH_KEY=`echo "$SSH_KEY" | tr " " "\n"`
SSH_KEY=$(cat <<-END
-----BEGIN RSA PRIVATE KEY-----
${SSH_KEY}
-----END RSA PRIVATE KEY-----
END
)

echo "${SSH_KEY}" > ~/.ssh/id_rsa
chmod 600 ~/.ssh/id_rsa
fi
git pull ${CLONE_URL};
java -Xms128M -Xmx${SERVER_MEMORY}M -jar ${SERVER_JARFILE}
