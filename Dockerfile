FROM quay.io/pterodactyl/core:java-glibc
USER root
ADD run.sh /
RUN chmod +x /run.sh; apk add openssh-client
USER container
